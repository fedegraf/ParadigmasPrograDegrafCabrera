﻿namespace Game
{
    class PlayerData
    {
        private Vector2 position;
        private float realheight;
        private float realwidht;
        private float scale;
        private float speed;
        private HealthController health;
        private Texture texture;

        public Texture Texture { get => texture; set => texture = value; }
        public Vector2 Position { get => position; set => position = value; }
        public float RealHeight { get => realheight; set => realheight = value * Scale; }
        public float RealWidht { get => realwidht; set => realwidht = value * Scale; }
        public float Scale { get => scale; set => scale = value; }
        public float OffSetY { get => RealHeight / 2; }
        public float OffSetX { get => RealWidht / 2; }
        public float Speed { get => speed; set => speed = value; }
        public Vector2 Size { get => new Vector2(RealWidht, RealHeight); }

        public HealthController Health { get => health; set => health = value; }
    }
}
