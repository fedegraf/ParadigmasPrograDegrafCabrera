﻿namespace Game
{
    public class Tile
    {
        public int xToDrawTile;
        public int yToDrawTile;
        public Texture texture;
        public Vector2 Size;
        public Vector2 Posiiton;
        public Tile(int XPosition, int YPosition, Texture texture)
        {
            xToDrawTile = XPosition;
            yToDrawTile = YPosition;
            this.texture = texture;
            Size = new Vector2(texture.Width, texture.Height);
            Posiiton = new Vector2(xToDrawTile, yToDrawTile);
        }
    }
}
