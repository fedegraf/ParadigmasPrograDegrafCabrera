﻿namespace Game
{
    public class Render
    {
        protected Texture texture;
        protected float realheight;
        protected float realwidht;
        protected Vector2 size;
        public float RealHeight { get => realheight; set => realheight = value; }
        public float RealWidht { get => realwidht; set => realwidht = value; }
        public float OffSetY { get => size.Y / 2; }
        public float OffSetX { get => size.X / 2; }
        public Vector2 Size { get => size; set => size = value; }
        public Texture Textutre { get => texture; set => texture = value; }

        public Render(Texture texture, Vector2 scale)
        {
            this.Textutre = texture;
            RealWidht = texture.Width;
            RealHeight = texture.Height;
            Size = new Vector2(RealWidht * scale.X, RealHeight * scale.Y);
        }
        public void Renderize(Transform transform)
        {
            Engine.Draw(texture, transform.Position.X, transform.Position.Y, transform.Scale.X, transform.Scale.Y,
                transform.Rotation, OffSetX, OffSetY);
        }
    }
}
