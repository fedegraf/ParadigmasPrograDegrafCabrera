﻿using System;
using System.Collections.Generic;

namespace Game
{
    public class EnemyController : GameObject, IDamageable
    {
        private List<Animation> animations;
        private Animation currentAnimation;
        private static float ShootingCooldown, CoolingCurrentTime;
        private int currentHealth, maxHealth;
        private float speed;

        public float Speed { get => speed; set => speed = value; }
        public int MaxHealth { get => maxHealth; set => maxHealth = value; }

        public bool isDestroyed => throw new NotImplementedException();

        public EnemyController(float speed, int maxHealth, List<Animation> animations,
            Transform transform, Render render, Collider collider) : base(transform, render, collider)
        {
            Speed = speed;
            MaxHealth = maxHealth;
            currentHealth = MaxHealth;
            this.animations = animations;
            currentAnimation = GetAnimation("idleAnimation");
            ShootingCooldown = 0.5f;
            CoolingCurrentTime = 0.6f;
        }

        event Action<IDamageable> IDamageable.OnDestroy
        {
            add
            {
                throw new NotImplementedException();
            }

            remove
            {
                throw new NotImplementedException();
            }
        }

        public override void Update()
        {
            currentAnimation = GetAnimation("idleAnimation");
            currentAnimation.Update();
            Render.Textutre = currentAnimation.CurrentFrame;
        }
        public Animation GetAnimation(string Id)
        {
            for (int i = 0; i < animations.Count; i++)
            {
                if (animations[i].Id == Id)
                {
                    return animations[i];
                }
            }
            Engine.Debug($"No se encontro la animacion: {Id}");
            return null;
        }
        public bool CheckCollisitionWithTiles(List<Tile> Tiles)
        {
            bool IsColliding = false;
            for (int i = 0; i < Tiles.Count; i++)
            {
                IsColliding = CollisionsUtilities.isBoxingColliding(Transform.Position, Render.Size, Tiles[i].Posiiton, Tiles[i].Size);
                if (IsColliding)
                {
                    return IsColliding;
                }
            }
            return IsColliding;
        }
        public void GetDamage(int damage)
        {
            currentHealth -= damage;
            if (currentHealth <= 0)
            {
                currentHealth = 0;
                Destroy();

            }
        }
        public void OnDestroy()
        {
            isActive = false;
        }

        public void Destroy()
        {
            GameManager.Instance.ChangeGameState(GameManager.GameState.WinScreen);
        }
    }
}
