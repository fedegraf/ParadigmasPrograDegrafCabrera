﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public static class EnemyFactory
    {
        public enum Enemy
        {
            normal
        }
        public static EnemyController CreateEnemy(Enemy enemy, Vector2 Position)
        {
            float speedEnemy;
            int maxHealthEnemy;
            Vector2 scale = new Vector2(0.25f, 0.25f);
            Texture texture;
            Transform transformEnemy = new Transform(Position, 0, scale);
            Collider colliderEnemy = new Collider("enemy");
        switch (enemy)
            {
                case Enemy.normal:
                    speedEnemy = 50f;
                    maxHealthEnemy = 100;
                    texture = Engine.GetTexture("Textures/Enemy/Idle Animation/Idle_1.png");
                    Render renderEnemy = new Render(texture, scale);
                    return new EnemyController(speedEnemy, maxHealthEnemy, CreateNormalEnemyAnimation(), transformEnemy, renderEnemy, colliderEnemy);
                    break;
                default:
                    speedEnemy = 50f;
                    maxHealthEnemy = 100;
                    texture = Engine.GetTexture("Textures/Enemy/Idle Animation/Idle_1.png");
                    renderEnemy = new Render(texture, scale);
                    return new EnemyController(speedEnemy, maxHealthEnemy, CreateNormalEnemyAnimation(), transformEnemy, renderEnemy, colliderEnemy);
                    break;
            }
        }

        private static List<Animation> CreateNormalEnemyAnimation()
        {
            List<Animation> animations = new List<Animation>();


            List<Texture> idleFrames = new List<Texture>();
            for (int i = 1; i <= 10; i++)
            {
                idleFrames.Add(Engine.GetTexture($"Textures/Enemy/Idle Animation/Idle_{i}.png"));
            }
            Animation idleAnimation = new Animation("idleAnimation", idleFrames, 0.1f, true);
            animations.Add(idleAnimation);


            /*            List<Texture> walkframes = new List<Texture>();
                        for (int i = 1; i <= 12; i++)
                        {
                            walkframes.Add(Engine.GetTexture($"Textures/Character/Run Animation/walk_{i}.png"));
                        }
                        Animation runAnimation = new Animation("runAnimation", walkframes, 0.1f, true);
                        animations.Add(runAnimation);

            */
            return animations;
        }
    }
}
