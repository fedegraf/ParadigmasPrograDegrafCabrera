﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class SimpleMenuScreen
    {

        private string texturePath;
        public string Id => "Simmple_Menu_Scene";
        public string BackgroundTexturePath { get; set; }
        public SimpleMenuScreen(string texturePath)
        {
            BackgroundTexturePath = texturePath;
        }
        public void Initialize()
        {

        }
        public void Update()
        {
            if (Engine.GetKey(Keys.ESCAPE))
            {
                GameManager.Instance.ChangeGameState(GameManager.GameState.MainMenu);
            }
        }

        public void Render()
        {
            Engine.Draw(texturePath, 0, 0);
        }
    }
}
