﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class Credits : IScene
    {
        private string backgroundTexturePath;
        public GameManager.GameState Id => GameManager.GameState.Credits;
        public string BackgroundTexturePath { get => backgroundTexturePath; set => backgroundTexturePath = value; }

        public Credits(string backgroundTexturePath)
        {
            BackgroundTexturePath = backgroundTexturePath;
        }

        public void Initialize()
        {

        }
        public void Update()
        {
            if (Engine.GetKey(Keys.ESCAPE))
            {
                GameManager.Instance.ChangeGameState(GameManager.GameState.MainMenu);
            }
        }
        public void Render()
        {
            Engine.Draw(backgroundTexturePath);
        }
    }
}
