﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class MainMenu : IScene
    {
        private const float INPUT_DELAY_TIME = 0.2f;

        private string backgroundTexturePath;
        private Button playButton;
        private Button creditsButton;
        private Button quitButton;
        private Button selectedButton;
        private float currentInputDelayTime;
        public GameManager.GameState Id => GameManager.GameState.MainMenu;

        public string BackgroundTexturePath { get => backgroundTexturePath; set => backgroundTexturePath = value; }
        public MainMenu(string backgroundTexturePath)
        {
            BackgroundTexturePath = backgroundTexturePath;

        }

        public void Initialize()
        {
            playButton = new Button(new Vector2(1,-150),1, "Png/Buttons/Play.png", "Png/Buttons/Play-.png");
            creditsButton = new Button(new Vector2(1, -20), 1, "Png/Buttons/Credits.png", "Png/Buttons/Credits-.png");
            quitButton = new Button(new Vector2(1, 120), 1, "Png/Buttons/Quit.png", "Png/Buttons/Quit-.png");

            playButton.AssignButtons(creditsButton, quitButton);
            creditsButton.AssignButtons(quitButton, playButton);
            quitButton.AssignButtons(playButton, creditsButton);

            SelectButton(playButton);
        }
        public void Update()
        {
            currentInputDelayTime += Time.deltaTime;


            playButton.Update();
            creditsButton.Update();
            quitButton.Update();

            if ((Engine.GetKey(Keys.UP) || Engine.GetKey(Keys.W))  && currentInputDelayTime >= INPUT_DELAY_TIME )
            {
                currentInputDelayTime = 0;
                SelectButton(selectedButton.PreviousButton);
            }

            if ((Engine.GetKey(Keys.DOWN) || Engine.GetKey(Keys.S)) && currentInputDelayTime >= INPUT_DELAY_TIME)
            {
                currentInputDelayTime = 0;
                SelectButton(selectedButton.NextButton);
            }

            if (Engine.GetKey(Keys.SPACE) && currentInputDelayTime >= INPUT_DELAY_TIME)
            {
                currentInputDelayTime = 0;
                PressSelectedButton();
            }
        }
        public void Render()
        {
            Engine.Draw(backgroundTexturePath);

            playButton.Render();
            creditsButton.Render();
            quitButton.Render();
        }

        private void SelectButton(Button button)
        {
            if(selectedButton != null)
            {
                selectedButton.UnSelect();
            }
            selectedButton = button;
            selectedButton.Select(); 
        }

        private void PressSelectedButton()
        {
            if (selectedButton != null)
            {
                if (selectedButton == playButton)
                {
                    GameManager.Instance.ChangeGameState(GameManager.GameState.Level);
                }
                else if (selectedButton == creditsButton)
                {
                    GameManager.Instance.ChangeGameState(GameManager.GameState.Credits);

                }
                else if (selectedButton == quitButton)
                {
                    GameManager.Instance.ExitGame();
                }
            }
        }
    }

}
