﻿
namespace Game
{
    public delegate void LifeDelegate();
    class HealthController
    {
        private int maxHealth;
        private int currentHealth;
        private bool isAlive;
        public int MaxHealth { get => maxHealth; private set => maxHealth = value; }
        public int CurrentHealth { get => currentHealth; private set => currentHealth = value; }
        public bool IsAlive { get => isAlive; private set => isAlive = value; }
        public LifeDelegate OnDeath;

        public HealthController(int maxhealt)
        {
            MaxHealth = maxhealt;
            CurrentHealth = maxhealt;
            IsAlive = true;
        }
        public void GetDamage(int damage)
        {
            if (isAlive)
            {
                CurrentHealth -= damage;

                if (currentHealth <= 0)
                {
                    CurrentHealth = 0;
                    Death();
                }

            }
        }
        public void Death()
        {
            IsAlive = false;
<<<<<<< HEAD
            OnDeath?.Invoke();
=======
            //Tambien deberia invocar el evento para mostrar la pantalla de derrota
>>>>>>> 4bd2e85a002c168b505a027632b17badea8f9807
        }
        public void Healed(int recovery)
        {
            CurrentHealth += recovery;

            if (currentHealth > maxHealth)
            {
                CurrentHealth = maxHealth;
            }
        }
    }
}