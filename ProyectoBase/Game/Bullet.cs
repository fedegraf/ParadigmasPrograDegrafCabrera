﻿

namespace Game
{
    class Bullet : GameObject
    {
        private float speed;
        private int damage;

        public float Speed { get => speed; set => speed = value; }
        public int Damage { get => damage; set => damage = value; }

        public Bullet(float speed, int damage,
            Transform transform, Render render, Collider collider) : base(transform, render, collider)
        {
            Speed = speed;
            Damage = damage;
        }

        public override void Update()
        {
            Transform.Position += new Vector2(1,0) * speed * Time.DeltaTime;
        }
    }
}
