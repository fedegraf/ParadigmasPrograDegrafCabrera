﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class Level : IScene
    {
        private string backgroundTexturePath;
        private static PlayerController player;
        private static EnemyController enemy;
        private static List<Tile> listOfTiles = new List<Tile>();
        private static List<Bullet> listOfBulllets = new List<Bullet>();
        public static List<GameObject> ActiveGameObjects = new List<GameObject>();

        public string BackgroundTexturePath { get => backgroundTexturePath; set => backgroundTexturePath = value; }
        public GameManager.GameState Id => GameManager.GameState.Level;
        public void Initialize()
        {
            BackgroundTexturePath = "Textures/Background.png";
            float speedPlayer = 50f;
            int maxHealthPlayer = 100;
            Transform transformPlayer = new Transform(new Vector2(50, 0), 0, new Vector2(0.25f, 0.25f));
            Render renderPlayer = new Render(Engine.GetTexture("Textures/Character/idle_1.png"), new Vector2(0.25f, 0.25f));
            Collider colliderPlayer = new Collider("player");
            player = new PlayerController(speedPlayer, maxHealthPlayer, CreatePlayerAnimation(), transformPlayer, renderPlayer, colliderPlayer);
            ActiveGameObjects.Add(player);
            Texture tileTexture = Engine.GetTexture("Textures/Platform_Tile.png");
            CreateTiles(1, tileTexture, 250, 0);
            CreateTiles(2, tileTexture, 375, 400);
            enemy = EnemyFactory.CreateEnemy(EnemyFactory.Enemy.normal, new Vector2(700, 290));
            ActiveGameObjects.Add(enemy);
        }
        public void Update()
        {
            foreach (GameObject gameObject in ActiveGameObjects)
            {
                if (gameObject.isActive)
                {
                    gameObject.Update();
                }
            }
            if (!player.CheckCollisitionWithTiles(Level.listOfTiles))
            {
                player.Fall();
            }
            if (listOfBulllets.Count != 0)
            {
                foreach (Bullet bullet in listOfBulllets)
                {
                    if (bullet.isActive)
                    {
                        bullet.Update();
                        foreach (GameObject gameObject in ActiveGameObjects)
                        {
                            string layerOfCollition = bullet.Collider.CheckCollitions(bullet, gameObject);
                            if (layerOfCollition == "player")
                            {
                                player.GetDamage(bullet.Damage);
                                bullet.isActive = false;
                            }
                            else if (layerOfCollition == "enemy")
                            {
                                enemy.GetDamage(bullet.Damage);
                                bullet.isActive = false;
                            }
                        }

                    }
                }
            }
            if (Engine.GetKey(Keys.F1))
            {
                GameManager.Instance.ChangeGameState(GameManager.GameState.WinScreen);
            }

            if (Engine.GetKey(Keys.F2))
            {
                GameManager.Instance.ChangeGameState(GameManager.GameState.GameOverScreen);
            }
        }
        public void Render()
        {
            Engine.Draw(backgroundTexturePath, 0, 0, 0.75f, 0.75f);
            DrawTiles(3);
            foreach (GameObject gameObject in ActiveGameObjects)
            {
                if (gameObject.isActive)
                {
                    gameObject.Render.Renderize(gameObject.Transform);
                }    
            }
            foreach (Bullet bullet in listOfBulllets)
            {
                if (bullet.isActive)
                {
                    bullet.Render.Renderize(bullet.Transform);
                }
            }
        }

        private static List<Animation> CreatePlayerAnimation()
        {
            List<Animation> animations = new List<Animation>();


            List<Texture> idleFrames = new List<Texture>();
            for (int i = 1; i <= 1; i++)
            {
                idleFrames.Add(Engine.GetTexture($"Textures/Character/idle_{i}.png"));
            }
            Animation idleAnimation = new Animation("idleAnimation", idleFrames, 1f, true);
            animations.Add(idleAnimation);


            List<Texture> walkframes = new List<Texture>();
            for (int i = 1; i <= 12; i++)
            {
                walkframes.Add(Engine.GetTexture($"Textures/Character/Run Animation/walk_{i}.png"));
            }
            Animation runAnimation = new Animation("runAnimation", walkframes, 0.1f, true);
            animations.Add(runAnimation);


            return animations;
        }
        private static void DrawTiles(int cantTiles)
        {
            listOfTiles.ForEach(tile =>
            Engine.Draw(tile.texture, tile.xToDrawTile, tile.yToDrawTile, 1, 1, 0, tile.Size.X / 2, tile.Size.Y / 2)
            );
        }
        private static void CreateTiles(int cantTiles, Texture texture, int YPosition, int XpositionStart)
        {
            int PositionX = XpositionStart;
            for (int i = 0; i <= cantTiles; i++)
            {
                Tile tile = new Tile(PositionX, YPosition, texture);
                listOfTiles.Add(tile);
                PositionX += texture.Width;
            }
        }
        public static void InstantiateBullet(Vector2 position, Vector2 size)
        {
            float spawnWidth = (position.X + size.X);
            Transform bulletTransform = new Transform(new Vector2(spawnWidth, position.Y), 0, new Vector2(0.1f, 0.1f));
            float speed = 150f;
            int damage = 50;
            Render bulletRender = new Render (Engine.GetTexture("Textures/Bullet.png"), new Vector2(0.1f, 0.1f));
            Collider bulletCollider = new Collider("bullet");
            Bullet myBullet = new Bullet(speed, damage, bulletTransform, bulletRender, bulletCollider); 
            listOfBulllets.Add(myBullet);

        }
        public static void GameOverHandler()
        {
            
        }
    }
}
