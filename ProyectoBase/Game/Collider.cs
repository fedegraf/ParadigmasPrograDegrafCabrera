﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public class Collider
    {
        private string layer;

        public string Layer { get => layer; set => layer = value; }

        public Collider(string layer)
        {
            Layer = layer;
        }

        public string CheckCollitions(GameObject gameObjectA, GameObject gameObjectB)
        {
            bool IsColliding = CollisionsUtilities.isBoxingColliding(gameObjectA.Transform.Position, gameObjectA.Render.Size,
                gameObjectB.Transform.Position, gameObjectB.Render.Size);
            if (IsColliding)
            {
                return gameObjectB.Collider.layer;
            }
            else
            {
                return "none";
            }
        }
    }
}
