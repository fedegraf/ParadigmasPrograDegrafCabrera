﻿using System.Collections.Generic;


namespace Game
{
    class PlayerController : GameObject, IDamageable
    {
        private List<Animation> animations;
        private Animation currentAnimation;
        private static float ShootingCooldown, CoolingCurrentTime;
        private int currentHealth, maxHealth;
        private float speed;
        public event System.Action<IDamageable> OnDestroy;

        public bool IsDestroy { get; private set; }
        public bool isDestroyed { get; set; }
        public float Speed { get => speed; set => speed = value; }
        public int MaxHealth { get => maxHealth; set => maxHealth = value; }
        public PlayerController(float speed, int maxHealth, List<Animation> animations,
            Transform transform, Render render, Collider collider) : base (transform, render, collider)
        {
            this.animations = animations;
            Speed = speed;
            MaxHealth = maxHealth;
            currentHealth = MaxHealth;
            currentAnimation = GetAnimation("idleAnimation");
            ShootingCooldown = 0.5f;
            CoolingCurrentTime = 0.6f;
        }
        public override void Update()
        {
            CoolingCurrentTime += Time.DeltaTime;
            checkInput();
            currentAnimation.Update();
            Render.Textutre = currentAnimation.CurrentFrame;
        }
        public void checkInput()
        {
            if (Engine.GetKey(Keys.D))
            {
                Transform.Position += new Vector2(Speed * Time.DeltaTime, 0);
                currentAnimation = GetAnimation("runAnimation");
                
            }
            else if (Engine.GetKey(Keys.A))
            {
                Transform.Position -= new Vector2( Speed * Time.DeltaTime, 0);
                currentAnimation = GetAnimation("runAnimation");
            }
            else if (Engine.GetKey(Keys.S))
            {
                if (CoolingCurrentTime >= ShootingCooldown)
                {
                    Level.InstantiateBullet(Transform.Position, Render.Size);
                    CoolingCurrentTime = 0;
                }
            }
            else if (Engine.GetKey(Keys.W))
            {
            }
            else
            {
                currentAnimation = GetAnimation("idleAnimation");
            }
        }
        public Animation GetAnimation(string Id)
        {
            for (int i = 0; i < animations.Count; i++)
            {
                if (animations[i].Id == Id)
                {
                    return animations[i];
                }
            }
            Engine.Debug($"No se encontro la animacion: {Id}");
            return null;
        }
        public bool CheckCollisitionWithTiles(List<Tile> Tiles)
        {
            bool IsColliding = false;
            for (int i = 0; i < Tiles.Count; i++)
            {
                IsColliding = CollisionsUtilities.isBoxingColliding(Transform.Position, Render.Size, Tiles[i].Posiiton, Tiles[i].Size);
                if (IsColliding)
                {
                    return IsColliding;
                }
            }
            return IsColliding;
        }
            public void Fall()
        {
            Transform.Position += new Vector2(0, Speed * Time.DeltaTime);
        }
        public void GetDamage(int damage)
        {
            currentHealth -= damage;
            if (currentHealth <= 0)
            {
                currentHealth = 0;
                
            }
        }
        public void Destroy()
        {
            OnDestroy?.Invoke(this);
            IsDestroy = true;
            isActive = false;
        }

    }
}
